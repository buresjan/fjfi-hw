import numpy as np
from sklearn.metrics import pairwise_distances


class KMeans:

    def __init__(self, k, seed=None, max_iter=200):
        self.k = k
        self.seed = seed
        if self.seed is not None:
            np.random.seed(self.seed)
        self.max_iter = max_iter
        self._cluster_labels = None
        self._centroids = None

    def initialise_centroids(self, data):
        initial_centroids = np.random.permutation(data.shape[0])[:self.k]
        self._centroids = data[initial_centroids]

        return self._centroids

    def assign_clusters(self, data):
        if data.ndim == 1:
            data = data.reshape(-1, 1)

        dist_to_centroid = pairwise_distances(data, self._centroids, metric='euclidean')
        self._cluster_labels = np.argmin(dist_to_centroid, axis=1)

        return self._cluster_labels

    def update_centroids(self, data):
        self._centroids = np.array([data[self._cluster_labels == i].mean(axis=0) for i in range(self.k)])

        return self._centroids

    def predict(self, data):
        return self.assign_clusters(data)

    def fit(self, data):
        self._centroids = self.initialise_centroids(data)

        for i in range(self.max_iter):

            self._cluster_labels = self.assign_clusters(data)
            self._centroids = self.update_centroids(data)
            if i % 100 == 0:
                print("Running Model Iteration %d " % i)
        print("Model finished running")
        return self
