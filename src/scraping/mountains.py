import json
from datetime import date, datetime
from typing import List, Dict

import requests
from bs4 import BeautifulSoup


class Mountain:

    def __init__(self, name: str, height: int, first_date: date):
        self.name = name
        self.height = height
        self.date = first_date


class Scrape:

    def __init__(self, num_of_mountains: int):
        self.num_of_mountains = num_of_mountains

    def get_mountains(self, save_json: bool = False) -> List[Mountain]:
        mountains = []
        r = requests.get(url='https://cs.wikipedia.org/wiki/Seznam_nejvy%C5%A1%C5%A1%C3%ADch_hor')
        soup = BeautifulSoup(r.text, 'lxml')
        for tr in soup.find('tbody').find_all_next('tr')[1:]:
            if tr.find('span', 'sortkey'):
                if '-' in tr.find('span', 'sortkey').get_text().strip():
                    mountains.append(Mountain(
                        name=tr.find('a').attrs['title'].strip(),
                        height=int(tr.find_all('td')[2].get_text().strip()),
                        first_date=datetime.strptime(tr.find('span', 'sortkey').get_text().strip(), '%Y-%m-%d').date()))
        if save_json:
            with open('mountains.json', 'w') as f:
                json.dump([self._be_json_suitable(mount.__dict__) for mount in mountains[:self.num_of_mountains]], f, ensure_ascii=False)
        return mountains[:self.num_of_mountains]

    @staticmethod
    def _be_json_suitable(dictionary: Dict) -> Dict:
        dictionary['date'] = dictionary['date'].strftime('%Y-%m-%d')
        return dictionary


if __name__ == "__main__":
    mounts = Scrape(num_of_mountains=50).get_mountains(save_json=True)
    print(mounts)
