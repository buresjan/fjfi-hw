from time import time

import numpy as np
from matplotlib import pyplot as plt


def fib_lin(n: int) -> int:
    if n <= 0:
        return 0
    f = [1, 1]
    for i in range(2, n):
        f.append(f[i - 1] + f[i - 2])
    return f[n - 1]


def fib_exp(n: int) -> int:
    if n <= 0:
        return 0
    elif n == 1:
        return n
    else:
        return fib_exp(n - 1) + fib_exp(n - 2)


def plot_computation_time(n: int, save_fig: bool = False):
    lin_times, exp_times, fib_numbers = [], [], []
    for i in range(1, n + 1):
        fib_numbers.append(i)
        start = time()
        fib_lin(i)
        lin_times.append((time() - start) * 1000)

        start = time()
        fib_exp(i)
        exp_times.append((time() - start) * 1000)

    plt.subplots()
    index = np.arange(len(fib_numbers))
    bar_width, opacity = 0.35, 0.8
    plt.bar(index, exp_times, bar_width, alpha=opacity, color='b', label='Recursion')
    plt.bar(index + bar_width, lin_times, bar_width, alpha=opacity, color='g', label='Dynamic Programming')
    plt.xlabel('Fibonacci number index')
    plt.ylabel('Computation time [ms]')
    plt.title('Computation Time of Fibonacci Numbers')
    plt.xticks(index + bar_width, fib_numbers)
    plt.legend()
    plt.tight_layout()
    if save_fig:
        plt.savefig('fibonacci_computation_time.png')
    plt.show()


if __name__ == "__main__":
    plot_computation_time(15, save_fig=True)
