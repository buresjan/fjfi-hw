from typing import Any, Union


class Node:

    def __init__(self, data: Any):
        self.data = data
        self.next = None


# Vsimntete si, ze zde je "smer" ukazatelu od PRVNIHO k POSLEDNIMU prvku
class Queue:

    # Ukazatele na prvni a posledni prvek jsou inicializovany na None
    def __init__(self):
        self.front = self.rear = None

    # Kontrola prazdne fronty
    def is_empty(self) -> bool:
        return self.front is None

    # Pridani noveho prvku do fronty
    def add(self, data: Any):
        temp = Node(data)
        if self.rear is None:
            self.front = self.rear = temp
        else:
            self.rear.next = temp
            self.rear = temp

    # Vyjmuti prvniho vlozeneho prvku
    def pop(self) -> Union[Any, None]:
        if self.is_empty():
            return None
        else:
            temp = self.front
            self.front = temp.next
            if self.front is None:
                self.rear = None
            return temp.data

    # Zobrazi frontu
    def display(self):
        iter_node = self.front
        if self.is_empty():
            print("Empty Queue")
        else:
            while iter_node is not None:
                print(iter_node.data, " -> ", end=" ")
                iter_node = iter_node.next


if __name__ == '__main__':
    # Inicializace fronty
    q = Queue()

    # Pridani prvku do fronty
    q.add(10)
    q.add(20)
    q.add(30)
    q.add(40)
    q.add(50)

    # Vypis fronty
    q.display()

    # Vypis prvniho vlozeneho prvku
    print("\nFront element is ", q.front.data)

    # Vyjmuti poslednich dvou vlozenych prvku
    q.pop()
    q.pop()

    # Vypis modifikovane fronty
    q.display()

    # Vypis posledniho vlozeneho prvku
    print("\nRear element is ", q.rear.data)
