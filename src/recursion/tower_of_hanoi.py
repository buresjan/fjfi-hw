
def tower_of_hanoi(num_of_rods: int, from_rod: str, to_rod: str, aux_rod: str):
    if num_of_rods == 1:
        print('Move disk 1 from rod', from_rod, 'to rod', to_rod)
        return
    tower_of_hanoi(num_of_rods - 1, from_rod, aux_rod, to_rod)
    print('Move disk', num_of_rods, 'from rod', from_rod, 'to rod', to_rod)
    tower_of_hanoi(num_of_rods - 1, aux_rod, to_rod, from_rod)


if __name__ == '__main__':
    n = 3
    tower_of_hanoi(n, from_rod='A', to_rod='C', aux_rod='B')
