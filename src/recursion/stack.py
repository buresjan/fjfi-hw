from typing import Any, Union


class Node:

    def __init__(self, data: Any):
        self.data = data
        self.next = None


# Vsimntete si, ze zde je "smer" ukazatelu od POSLEDNIHO k PRVNIMU prvku
class Stack:

    # Head je defaultne nastaven na None
    def __init__(self):
        self.head = None

    # Kontrola prazdneho zasobniku
    def is_empty(self) -> bool:
        return self.head is None

    # Pridani noveho prvku do zasobniku
    def add(self, data: Any):
        if self.head is None:
            self.head = Node(data)
        else:
            new_node = Node(data)
            new_node.next = self.head
            self.head = new_node

    # Vyjmuti posledniho vlozeneho prvku
    def pop(self) -> Union[Any, None]:
        if self.is_empty():
            return None
        else:
            # Zde posuneme head na predposledni prvek a posledni nasmerujeme na None.
            # Python ma garbage collector implicitne, proto se nemusime starat a cisteni pameti!
            last_node = self.head
            self.head = self.head.next
            last_node.next = None
            return last_node.data

    # Vraceni hodnoty posledniho vlozeneho prvku bez jeho vyjmuti
    def peek(self) -> Union[Any, None]:
        return None if self.is_empty() else self.head.data

    # Zobrazi zasobnik
    def display(self):
        iter_node = self.head
        if self.is_empty():
            print("Empty Stack")
        else:
            while iter_node is not None:
                print(iter_node.data, " -> ", end=" ")
                iter_node = iter_node.next


if __name__ == "__main__":
    # Inicializace zasobniku
    s = Stack()

    # Pridani prvku do zasobniku
    s.add(1)
    s.add(2)
    s.add(3)
    s.add(4)

    # Vypis zasobniku
    s.display()

    # Vypis posledniho vlozeneho prvku
    print("\nTop element is ", s.peek())

    # Vyjmuti poslednich dvou vlozenych prvku
    s.pop()
    s.pop()

    # Vypis modifikovaneho zasobniku
    s.display()

    # Vypis posledniho vlozeneho prvku (ktery jeste nebyl vyjmut)
    print("\nTop element is ", s.peek())
