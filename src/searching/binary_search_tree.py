import pickle
from typing import Union, List


class Node:
    def __init__(self, value):
        self.left = None
        self.right = None
        self.value = value


def insert(root: Node, node: Node):
    if root is None:
        # noinspection PyUnusedLocal
        root = node
    else:
        if root.value < node.value:
            if root.right is None:
                root.right = node
            else:
                insert(root.right, node)
        else:
            if root.left is None:
                root.left = node
            else:
                insert(root.left, node)


def iter_insert(root: Node, node: Node) -> Node:
    temp = root
    while temp is not None:
        if node.value < temp.value:
            if temp.left is None:
                temp.left = node
                return temp.left
            temp = temp.left
        else:
            if temp.right is None:
                temp.right = node
                return temp.right
            temp = temp.right


def search(root: Node, key: int) -> Union[Node, None]:
    if root is None or root.value == key:
        return root

    if root.value < key:
        return search(root.right, key)

    return search(root.left, key)


def find_max(root: Node) -> Union[Node, None]:
    return root if root.right is None else find_max(root.right)


def find_min(root: Node) -> Union[Node, None]:
    return root if root.left is None else find_min(root.left)


def delete_node(root: Node, key) -> Union[Node, None]:
    if root is None:
        return root

    if key < root.value:
        root.left = delete_node(root.left, key)
    elif key > root.value:
        root.right = delete_node(root.right, key)
    else:
        if root.left is None:
            temp = root.right
            # noinspection PyUnusedLocal
            root = None
            return temp

        elif root.right is None:
            temp = root.left
            # noinspection PyUnusedLocal
            root = None
            return temp

        temp = find_min(root.right)
        root.value = temp.value
        root.right = delete_node(root.right, temp.value)

    return root


if __name__ == "__main__":

    r = Node(10)
    insert(r, Node(5))
    insert(r, Node(17))
    insert(r, Node(13))
    insert(r, Node(1))
    insert(r, Node(6))
    insert(r, Node(7))

    # using pickle
    with open('root.p', 'wb') as f:
        pickle.dump(r, f)
    with open('root.p', 'rb') as e:
        t = pickle.load(e)

    # using our custom methods
    out = []

    def order(root: Node) -> List[int]:
        if root:
            # be aware of order
            out.append(root.value)
            order(root.left)
            order(root.right)
        return out

    order(r)
    print(out)

    new_root = Node(out[0])

    def get_root(data: List[int]):
        for value in data[1:]:
            insert(new_root, Node(value))

    get_root(out)
    print(new_root.right.value)
