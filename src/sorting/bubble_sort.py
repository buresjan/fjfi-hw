from typing import List


def bubble_sort(arr: List[int]) -> List[int]:
    arr = arr.copy()  # copy() zde davame z duvodu, ze List[] je mutable, viz https://medium.com/@meghamohan/mutable-and-immutable-side-of-python-c2145cf72747
    n = len(arr)
    for i in range(n):
        for j in range(0, n - i - 1):  # tento for cyklus primo implementuje myslenku bubble sortu (prochazim postupne pole z jedne strany, porovnavam a prehazuji)
            if arr[j] > arr[j + 1]:
                arr[j], arr[j + 1] = arr[j + 1], arr[j]
    return arr


def optimized_bubble_sort(arr: List[int]) -> List[int]:
    arr = arr.copy()
    n = len(arr)
    for i in range(n):
        swapped = False  # definuji si "zarazku", ktera mi rika, jestli jsem v dane iteraci vubec neco menil nebo ne
        for j in range(0, n - i - 1):
            if arr[j] > arr[j + 1]:
                arr[j], arr[j + 1] = arr[j + 1], arr[j]
                swapped = True
        if not swapped:  # pokud v teto iteraci zadna zamena prvku neprobehla tak ukonci
            break
    return arr


if __name__ == "__main__":
    array = [100, 30, 50, 20, 70, 40, 90]

    print(f'An array to be sorted: \n\t{array}\n')

    print(f'Bubble sort: \n\t{bubble_sort(array)}\n')
    print(f'Optimized bubble sort: \n\t{optimized_bubble_sort(array)}\n')

    print(f'The original array stated above: \n\t{array}')  # pokud zakomentujete radek 5 nebo 15, tak uvidite co jak funguje princip mutable/immutable v Pythone
