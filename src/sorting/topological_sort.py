from collections import defaultdict
from typing import List


class Graph:
    def __init__(self, num_of_vertices: int):
        self.graph = defaultdict(list)  # dictionary obsahujici seznam spojeni jednotlivych vrcholu, zde si importujeme extra kod a setrime tak praci ;)
        self.num_of_vertices = num_of_vertices

    def add_edge(self, u: int, v: int):  # metoda na pridavani hran do grafu
        self.graph[u].append(v)

    def _topological_sort(self, v: int, visited: List[bool], stack: List[int]):  # pomocna rekurzivni metoda, ktera se pouziva v hlavni metode nize (proto ji uvozujeme podtrzitkem - chapejte to jako privatni metodu v C++)
        visited[v] = True  # oznacime prislusny vrchol, ze jsme ho jiz prochazeli (prislusne pole v listu zmenime na True)
        for i in self.graph[v]:  # projde rekurentne vsechny vrcholy spojene s timto vrcholem v
            if not visited[i]:
                self._topological_sort(i, visited, stack)
        stack.insert(0, v)  # ulozi vrchol v do zasobniku na nultou pozici

    def topological_sort(self) -> List[int]:  # hlavni metoda na topologicke razeni
        visited = [False] * self.num_of_vertices  # na zacatku oznacim vsechny vrcholy jako jeste neprojite (nezpracovane radicim algoritmem)
        stack = []
        for i in range(self.num_of_vertices):  # for cyklus prochazi vsechny vrcholy a vola na ne pomocnou radici/tridici funkci
            if not visited[i]:
                self._topological_sort(i, visited, stack)
        return stack.copy()


if __name__ == "__main__":
    # Inicializace tohoto grafu s 6 vrcholy: 0, 1, 2, 3, 4, 5
    g = Graph(6)

    # Pridani prislusnych hran podle: https://media.geeksforgeeks.org/wp-content/cdn-uploads/graph.png
    g.add_edge(5, 2)
    g.add_edge(5, 0)
    g.add_edge(4, 0)
    g.add_edge(4, 1)
    g.add_edge(2, 3)
    g.add_edge(3, 1)

    print(f'Ordered graph: {g.topological_sort()}')
