from typing import List


def insertion_sort(arr: List[int]) -> List[int]:
    arr = arr.copy()
    for i in range(1, len(arr)):

        key = arr[i]

        # Zde posouvame prvky arr[0..i-1], ktere jsou vetsi nez key, a to vzdy o jedno pole "doprava"
        j = i - 1
        while j >= 0 and key < arr[j]:
            arr[j + 1] = arr[j]
            j -= 1
        arr[j + 1] = key  # Na konci kazdeho presunovani vlozime key na sve misto v  leve, jiz serazene, casti pole
    return arr


if __name__ == "__main__":
    array = [100, 30, 50, 20, 70, 40, 90]

    print(f'An array to be sorted: \n\t{array}\n')

    print(f'Insertion sort: \n\t{insertion_sort(array)}\n')

    print(f'The original array stated above: \n\t{array}')
