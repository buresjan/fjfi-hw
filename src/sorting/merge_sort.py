from typing import List


# V tomto algoritmu budeme postupovat tak, ze si nebudeme vytvaret kopii pole, ale budeme pracovat primo s objektem, ktery funkce dostane na vstupu.
def merge_sort(arr: List[int]):
    if len(arr) > 1:  # kdyz uvazujeme pole s jednom prvkem, neni co radit
        mid = len(arr) // 2  # najdeme "prostredek" pole (pisu "prostredek", protoze pouzivame // coz je celociselne deleni)
        l_arr = arr[:mid]  # pomoci slicingu deklarujeme novymi promennymi leve a prave "podpole", viz https://stackoverflow.com/questions/509211/understanding-slice-notation#509295
        r_arr = arr[mid:]  # dtto

        merge_sort(l_arr)  # rekurzivne volame merge sort na leve "podpole"
        merge_sort(r_arr)  # rekurzivne volame merge sort na prave "podpole"

        i = j = k = 0

        # zde se deje samotne razeni, kdyz rekurze dorazi na uroven jednotlivych prvku, zacne je zpet spojovat a pritom je postupne radi, viz https://upload.wikimedia.org/wikipedia/commons/e/e6/Merge_sort_algorithm_diagram.svg
        while i < len(l_arr) and j < len(r_arr):
            if l_arr[i] < r_arr[j]:
                arr[k] = l_arr[i]
                i += 1
            else:
                arr[k] = r_arr[j]
                j += 1
            k += 1

        # vyridi prebyvajici prvky, tedy pripad, kdy mame v l_arr rekneme N prvku a v r_arr (N + 1) prvku a naopak
        while i < len(l_arr):
            arr[k] = l_arr[i]
            i += 1
            k += 1

        while j < len(r_arr):
            arr[k] = r_arr[j]
            j += 1
            k += 1


if __name__ == '__main__':
    array = [100, 30, 50, 20, 70, 40, 90]

    print(f'An array to be sorted: \n\t{array}\n')

    merge_sort(array)

    print(f'The original array affected by merge sort: \n\t{array}\n')
