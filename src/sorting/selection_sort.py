from typing import List


def selection_sort(arr: List[int]) -> List[int]:
    arr = arr.copy()
    n = len(arr)
    for i in range(n):
        min_idx = i
        for j in range(i + 1, n):  # hledani minima v neserazene casti pole arr
            if arr[min_idx] > arr[j]:
                min_idx = j
            # prohozeni nalezeneho nejmensiho prvku s pivotem
        arr[i], arr[min_idx] = arr[min_idx], arr[i]
    return arr


if __name__ == "__main__":
    array = [100, 30, 50, 20, 70, 40, 90]

    print(f'An array to be sorted: \n\t{array}\n')

    print(f'Selection sort: \n\t{selection_sort(array)}\n')
