from src.recursion.fibonacci import fib_exp, fib_lin


def test_fib_exp():
    assert fib_exp(0) == 0
    assert fib_exp(1) == 1
    assert fib_exp(2) == 1
    assert fib_exp(10) == 55


def test_fib_lin():
    assert fib_lin(0) == 0
    assert fib_lin(1) == 1
    assert fib_lin(2) == 1
    assert fib_lin(10) == 55
