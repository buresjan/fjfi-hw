from src.recursion.queue import Queue
from src.recursion.stack import Stack


def test_queue():
    q = Queue()
    q.add(10)
    q.add(20)

    assert 10 == q.pop()
    assert 20 == q.pop()
    assert q.pop() is None


def test_stack():
    s = Stack()
    s.add(10)
    s.add(20)

    assert 20 == s.pop()
    assert 10 == s.pop()
    assert s.pop() is None
