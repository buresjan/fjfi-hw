from src.searching.binary_search_tree import Node, insert, find_min, find_max, search, delete_node


def test_insert():
    r = Node(10)
    insert(r, Node(5))
    insert(r, Node(20))
    insert(r, Node(15))

    # check the left side
    assert r.left.value == 5
    assert r.left.left is None
    assert r.left.right is None

    # check the right side
    assert r.right.value == 20
    assert r.right.left.value == 15
    assert r.right.right is None
    assert r.right.left.left is None
    assert r.right.left.right is None


def test_iter_insert():
    r = Node(10)
    insert(r, Node(5))
    insert(r, Node(20))
    insert(r, Node(15))

    # check the left side
    assert r.left.value == 5
    assert r.left.left is None
    assert r.left.right is None

    # check the right side
    assert r.right.value == 20
    assert r.right.left.value == 15
    assert r.right.right is None
    assert r.right.left.left is None
    assert r.right.left.right is None


def test_find_min():
    r = Node(10)
    insert(r, Node(5))
    insert(r, Node(20))
    insert(r, Node(15))

    t = find_min(r)
    assert t.value == 5


def test_find_max():
    r = Node(10)
    insert(r, Node(5))
    insert(r, Node(20))
    insert(r, Node(15))

    t = find_max(r)
    assert t.value == 20


def test_search():
    r = Node(10)
    insert(r, Node(5))
    insert(r, Node(20))
    insert(r, Node(15))

    assert search(r, 5)
    assert not search(r, 7)


def test_delete_node():
    r = Node(10)
    insert(r, Node(5))
    insert(r, Node(20))
    insert(r, Node(15))

    delete_node(r, 7)
    assert r.left.value == 5

    delete_node(r, 5)
    assert r.left is None
