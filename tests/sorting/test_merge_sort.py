from src.sorting.merge_sort import merge_sort


def test_merge_sort():
    input_array = [5, 6, 7, 3, 4, 2, 9, 1, 8]
    output_array = [1, 2, 3, 4, 5, 6, 7, 8, 9]

    merge_sort(input_array)

    assert output_array == input_array
