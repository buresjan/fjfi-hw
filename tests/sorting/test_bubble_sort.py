from src.sorting.bubble_sort import bubble_sort, optimized_bubble_sort


def test_bubble_sort():
    input_array = [5, 6, 7, 3, 4, 2, 9, 1, 8]
    output_array = [1, 2, 3, 4, 5, 6, 7, 8, 9]

    assert output_array == bubble_sort(input_array)
    assert output_array == optimized_bubble_sort(input_array)
