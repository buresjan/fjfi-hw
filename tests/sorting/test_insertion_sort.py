from src.sorting.insertion_sort import insertion_sort


def test_insertion_sort():
    input_array = [5, 6, 7, 3, 4, 2, 9, 1, 8]
    output_array = [1, 2, 3, 4, 5, 6, 7, 8, 9]

    assert output_array == insertion_sort(input_array)
