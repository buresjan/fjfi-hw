from src.sorting.topological_sort import Graph


def test_topological_sort():
    g = Graph(6)

    g.add_edge(5, 2)
    g.add_edge(5, 0)
    g.add_edge(4, 0)
    g.add_edge(4, 1)
    g.add_edge(2, 3)
    g.add_edge(3, 1)

    assert [5, 4, 2, 3, 1, 0] == g.topological_sort()
